package test;

import java.util.Scanner;

import be.belgium.eid.eidlib.BeID;
import be.belgium.eid.event.CardAdapter;
import be.belgium.eid.exceptions.EIDException;

public class eID_Reader {

	public static void main(String[] args) {
	    try {
	      final BeID eID = new BeID(true);  
	      eID.enableCardListener(new CardAdapter() {
	        public void cardInserted() {

	  	        System.out.println("> CARD INSERTED");
	          try {
		  	    System.out.println("Firstname: " + eID.getIDData().get1stFirstname().toString());
		        System.out.println("Lastname: " + eID.getIDData().getName().toString());
	          } catch (EIDException e) { 
	            System.err.println("> " + e.getMessage());
	          } catch (Exception e) {
	            System.err.println("> " + e.getMessage());
	          }

	        }
	        
	        public void cardRemoved() {

	  	        System.out.println("> CARD REMOVED");
	        }
	      });

	      System.out.println("> type QUIT to end program.");
	      String input = "";
	      while (!input.equalsIgnoreCase("QUIT")) {
	        input = new Scanner(System.in).next();
	      }

	    } catch (Exception e) {
	      System.err.println("> " + e.getMessage());
	    }

	    }

}
